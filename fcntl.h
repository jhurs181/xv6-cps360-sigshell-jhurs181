#define O_RDONLY  0x0000
#define O_WRONLY  0x0001
#define O_RDWR    0x0002
#define O_CREATE  0x0200
#define O_EXEC    0x1000
#define O_LSDIR   0x0004  // r - list directory
