#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

static int const MAX_LINE_LENGTH = 128;

//available modes for the editor
enum Mode {
    normal,
    insert
};
typedef enum Mode Mode; //so we can use 'Mode' as a typename instead of 'enum Mode'

//doubly-linked list
struct node {
    char *line;
    struct node *next;
    struct node *prev;
};

//editor state
struct ed_state {
    struct node *ll; //pointer to the beginning of our linked-list buffer
    int length; //number of lines in the file
    Mode mode;
    int line_num;
    int edited;     // true if edited since the last save
    int lineNums;   // true if you want line numbers
    struct node *clipboard; // node of the line that is copied
    char *filename;
};
struct ed_state *ed; //global editor state

//return 1 if the string is empty
int str_empty(char * str) {
    int empty = 1;
    for(int i=0; i<strlen(str); i++) {
        if (*(str+i) != '\n' &&
            *(str+i) != '\r' &&
            *(str+i) != '\t' &&
            *(str+i) != ' ' &&
            *(str+i) != '\0')
        {
            empty = 0;
            break;
        }
    }
    return empty;
}

//insert into linked list
struct node * ll_insert(struct node * buf, struct node * new) {
    buf->next = new;
    new->prev = buf;
    ed->length++;
    return new;
}

//insert an empty line
struct node * ll_insert_empty(struct node * buf) {
    //create the empty line
    char el[2] = "\n";

    //create the node for the empty line
    struct node *els = malloc(sizeof(els)); 
    memset(els, 0, sizeof(els));

    //add the empty line to the node
    els->line = el; 
    els->next = 0;

    //put it in the linked list
    return ll_insert(buf, els); 
}

// inserts <cmd> before the current line and advances the current line
void ll_insert_current(char * cmd) {
    
    // create new null terminated line of max length
    char *linebuf = malloc(MAX_LINE_LENGTH);
    memset(linebuf, 0, sizeof(linebuf));
    int j;
    for(j = 0; j < strlen(cmd); j++) {
        linebuf[j] = cmd[j];
    }
    linebuf[j] = '\n';
    linebuf[j+1] = '\0';
    
    // create new node with the command as the line
    struct node * new_line = malloc(sizeof(*new_line));
    new_line->line = linebuf;

    struct node * line = ed->ll;
    struct node * next_line = line->next;

    // loop through lines till we get to the current one
    for(int i = 1; i < ed->line_num-1; i++) {
        line = next_line;
        next_line = next_line->next;
    }

    // insert new line and reset pointers
    if (ed->line_num == 1) {
        new_line = ll_insert(new_line, line);
        ed->ll = new_line->prev;
    } else {
        new_line = ll_insert(line, new_line);
        next_line->prev = new_line;
        new_line->next = next_line;
    }

    // navigate down one line
    if (ed->line_num < ed->length) 
        ed->line_num++;
}

//get a user's command
int getcmd(char *buf, int nbuf) {
  memset(buf, 0, nbuf);
  gets(buf, nbuf);
  if(buf[0] == 0) // EOF
    return -1;
  return 0;
}


//saves the file
void save() {
    unlink(ed->filename);
    int fp = open(ed->filename, O_CREATE | O_WRONLY);
    if (fp < 0) {
        printf(1, "There was an error saving the file %s\n", ed->filename);
    }
    else {
        struct node *buffer = ed->ll;
        if (buffer->line) {
            write(fp, buffer->line, strlen(buffer->line));
        }
        while (buffer->next) {
            buffer = buffer->next;
            if (buffer->line) {
                write(fp, buffer->line, strlen(buffer->line));
            }
        }
    }
    close(fp); 
}

//frees memory and exits the program.
void quit() {
    //free the memory pointed to by ed's linked list buffer
    struct node *free_buf1 = ed->ll;
    struct node *free_buf2 = free_buf1->next;
    free(free_buf1);
    while (free_buf2) {
        free_buf1 = free_buf2;
        free_buf2 = free_buf1->next;
        free(free_buf1);
    }
    //free the editor state 'object'
    free(ed);

    //leave the program
    exit();
}

//process a user command in normal mode
void process_nm_command(char *cmd) {

    //saving/quitting
    if (!strcmp(cmd, "s")) {        // save
        save();
    }
    else if (!strcmp(cmd, "q")) {   // quit
        if (!ed->edited)
            quit();
    }
    else if (!strcmp(cmd, "sq")) {  // save & quit
        save();
        quit();
    }
    else if (!strcmp(cmd, "wq")) {  // quit w/o saving
        quit();
    }

    //navigation
    else if (!strcmp(cmd, "j")) {   //navigate down one line
        if (ed->line_num < ed->length) 
            ed->line_num++;
    }
    else if (!strcmp(cmd, "k")) {   //navigate up one line
        if (ed->line_num > 0)
            ed->line_num--;
    }
    else if (*cmd == 'g' && *(cmd+1) == ' ') { //goto line number
        //convert to integer
        int ln = atoi(&cmd[2]);

        //check if it is in bounds
        if (ln >= 0 && ln <= ed->length) {
            //set current line number
            ed->line_num = ln;
        }
    }

    //deletion
    else if (*cmd == 'd' && *(cmd+1) == ' ') { //delete line
        //convert to integer
        int ln = atoi(&cmd[2]);

        //check if it is in bounds
        if (ln > 0 && ln <= ed->length) {
            //delete line
            if (ln == 1) { //special case at first line
                struct node * buf = ed->ll;
                ed->ll = (ed->ll)->next;
                free(buf);
                (ed->ll)->prev = 0;
            }
            else if (ln == ed->length) { //special case at last line
                struct node * buf = ed->ll;
                for (int i=0; i<ln-2; i++) {
                    buf = buf->next;
                }
                free(buf->next);
                buf->next = 0;
            }
            else { //general case
                struct node * buf = ed->ll;
                //move to the node before the one we want to delete
                for (int i=0; i<ln-2; i++) { 
                    buf = buf->next;
                }
                struct node * prev = buf; //save it
                buf = buf->next; 
                struct node * next = buf->next; //save a ptr to the node after
                free(buf); //delete the line
                prev->next = next; //move pointers around to patch up the list
                next->prev = prev;
            }
            ed->length--;
        }
    }
}

// process a user command in insert mode
void process_im_command(char *cmd) {
    if (!strcmp(cmd, "c")) { // TODO: this won't work because you have to have a line number given after c
        // TODO: copy the line given to a clipboard variable (copy the node?)
    }
    else if (!strcmp(cmd, "p")) { // TODO: this won't work becouse you have to have a line number given after p
        // TODO: paste the clipboard contents before the line given
    }
    else { // insert a line (cmd) before the current line
        ll_insert_current(cmd);
    }
}

//print the current editor state
void print_state() {
    char* msg = "";
    if (ed->mode == insert)
        msg = "insert";
    else if (ed->mode == normal)
        msg = "normal";
    else
        msg = "something else";

    printf(1, "Current mode: %s", msg);
    printf(1, "\nCurrent line number: %d\n", ed->line_num);
    printf(1, "Buffer pointer: %d\n", ed->ll);
}

// prints a help message
void print_help() {
    printf(1, "==================================================== ed help:\n"
            "|  General Commands:\n"
            "|      .   switch to normal mode\n"
            "|      i   switch to insert mode\n"
            "|      ln  toggle line numbers\n"
            "|      h   list ed commands\n"
            "|      st  list ed state\n"
            "|\n"
            "|  Normal Mode------------\n"
            "|      q       quit\n"
            "|      s       save\n"
            "|      sq      save and quit\n"
            "|      wq      quit w/o saving\n"
            "|      g ##    goto line number (set current line number)\n"
            "|      j       decrement line number\n"
            "|      k       increment line number\n"
            "|      d ##    delete line at ##\n"
            "|\n"
            "|  Insert Mode------------\n"
            "|      c ##    copy line number\n"
            "|      p ##    paste before line number\n"
            "|      d ##    delete line number\n"
            "|      {text}  write new line at current position\n"
            "====================================================\n"
    );
}

// prints the file to the console
void print_file() {

    // TODO: logic for how much to print in the terminal
    
    int ln = 1;
    struct node *buffer = ed->ll;
    if (buffer->line) {
        char* cur = (ed->line_num == ln) ? ">" : " " ;
        if (ed->lineNums) { // if line numbers is turned on
            printf(1, "%s   %d  %s", cur, ln, buffer->line);
            ++ln;
        }
        else {
            printf(1, "%s%s", cur, buffer->line);
        }
    }
    while (buffer->next) {
        buffer = buffer->next;
        if (buffer->line) {
            char* cur = (ed->line_num == ln) ? ">" : " " ;
            if (ed->lineNums) { // if line numbers is turned on
                char* spaces = (ln < 10) ? "   " // TODO - can I put a tab character instead?
                             : (ln < 100) ? "  "
                             : (ln < 1000) ? " "
                             : "" ; // handle line number spacing
                printf(1, "%s%s%d  %s", cur, spaces, ln, buffer->line);
                ++ln;
            }
            else {
                printf(1, "%s%s", cur, buffer->line);
            }
        }
    }
}


int main(int argc, char *argv[])
{
    int fp;
    struct node *buf = malloc(sizeof(*buf));
    memset(buf, 0, sizeof(buf));

    //set beginning editor state
    ed = malloc(sizeof(*ed));
    ed->line_num = 1;
    ed->lineNums = 1;
    ed->ll = buf; //save a ptr to the beginning of the list

    if (argc > 1) {
        //open file and read it into the buffer
        ed->filename = argv[1];

        fp = open(ed->filename, O_RDONLY);
        if (fp < 0) {
            printf(1, "There was an error opening file %s\n", ed->filename);
            quit();
        }
        else {
            char c;
            char *linebuf;
            int i, cc, eof=0;

            while(!eof) {
                //read one character at a time until we hit the end of the line
                linebuf = malloc(MAX_LINE_LENGTH);
                memset(linebuf, 0, sizeof(linebuf));
                for (i=0; i+1<MAX_LINE_LENGTH; i++) {
                    cc = read(fp, &c, 1);
                    if (cc < 1 || c == '\0') {
                        eof = 1;
                        if (c != '\n') {
                            linebuf[i] = '\n';
                        }
                        break;
                    }
                    linebuf[i] = c;
                    if (c == '\n' || c == '\r') {
                        break;
                    }
                }

                //null-terminate the string and store in the node
                linebuf[i+1] = '\0';
                buf->line = linebuf;

                //create a new node, set pointers, and make buf point to the new node
                struct node *newbuf = malloc(sizeof(newbuf));
                buf = ll_insert(buf, newbuf);
            }

            //add an empty line if previous line was not empty
            if (str_empty(linebuf) == 0) {
                buf = ll_insert_empty(buf);
            }
        }
        close(fp);
    }

    //loop waiting for user input
    char *command = malloc(MAX_LINE_LENGTH);
    memset(command, 0, sizeof(command));

    while (1) {
        //(re)print file
        print_file();
        char* m = (ed->mode == normal) ? "n" : "i" ;
        printf(1, "\n%s> ", m);

        //read from command line
        getcmd(command, MAX_LINE_LENGTH);
        command[strlen(command)-1] = 0;  // chop \n

        // handle mode switching or ...
        if (!strcmp(command, ".")) {
            ed->mode = normal;
        }
        else if (!strcmp(command, "i")) {
            ed->mode = insert;
        }

        // ... do general settings or ...
        else if (!strcmp(command, "h"))
            print_help();
        else if (!strcmp(command, "st"))
            print_state();
        else if (!strcmp(command, "ln")) {
            ed->lineNums = (ed->lineNums) ? 0 : 1 ;
        }

        // ... process user command
        else if (ed->mode == normal)
            process_nm_command(command);
        else if (ed->mode == insert)
            process_im_command(command);

    } //end user input loop
    
    //no exit() because that will be called when user enters 'q'
}
